#POW
def power(n, c):
    result = 1
    while (c):
        if (c&1):
            result=result*n
        n = n*n
        c >>=1
    return result
    
def compare(a,b,c):
    n = a
    d = power(n,c)
    n = b
    e = power(n,c)
    
    if (d < e):
        print("<")
    elif (d == e):
        print("=")
    else:
        print(">")
    
compare(-7,7,2)